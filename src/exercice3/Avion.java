package exercice3;

public class Avion {
	private String modele;
	private int masseVide;
	private Passager[] passagers;
	
	public Avion(String modele, int masseVide, Passager[] passagers) {
		this.modele = modele;
		this.masseVide = masseVide;
		this.passagers = passagers;
	}

	public String getModele() {
		return modele;
	}

	public void setModele(String modele) {
		this.modele = modele;
	}

	public int getMasseVide() {
		return masseVide;
	}

	public void setMasseVide(int masseVide) {
		this.masseVide = masseVide;
	}

	public Passager[] getPassagers() {
		return passagers;
	}

	public void setPassagers(Passager[] passagers) {
		this.passagers = passagers;
	}
	
	public int getMasse() {
		int masse = this.getMasseVide();
		
		for (int i = 0; i < this.passagers.length; i++) {
		    masse = masse + this.passagers[i].getMasse();
		}
		
		return masse;
	}
}
