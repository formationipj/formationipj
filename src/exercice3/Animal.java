package exercice3;

public class Animal extends Passager {

	private String type;
	
	public static final int MASSE = 15;
	
	public Animal(String nom, String type) {
		super(nom);
		this.type = type;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getMasse() {
		return MASSE;
	}
}
