package exercice6;

import java.util.ArrayList;

public class Avion {
	private String modele;
	private int masseVide;
	private ArrayList<Chargeable> chargement;
	
	public Avion(String modele, int masseVide, ArrayList<Chargeable> chargement) {
		this.modele = modele;
		this.masseVide = masseVide;
		this.chargement = chargement;
	}

	public String getModele() {
		return modele;
	}

	public void setModele(String modele) {
		this.modele = modele;
	}

	public int getMasseVide() {
		return masseVide;
	}

	public void setMasseVide(int masseVide) {
		this.masseVide = masseVide;
	}
	
	public ArrayList<Chargeable> getChargement() {
		return chargement;
	}

	public void setChargement(ArrayList<Chargeable> chargement) {
		this.chargement = chargement;
	}

	public int getMasse() {
		int masse = this.getMasseVide();
		
		for (Chargeable c : this.chargement) {
		    masse = masse + c.getMasse();
		}
		
		return masse;
	}
}
