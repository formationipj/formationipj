package exercice6;

public class Valise implements Chargeable {
	
	private Passager passager;
	private int masse;
	
	public Valise(Passager passager, int masse) {
		this.passager = passager;
		this.masse = masse;
	}

	public Passager getPassager() {
		return passager;
	}

	public void setPassager(Passager passager) {
		this.passager = passager;
	}

	public int getMasse() {
		return masse;
	}

	public void setMasse(int masse) {
		this.masse = masse;
	}
}
