package exercice5;

public class Avion {
	private String modele;
	private int masseVide;
	private Chargeable[] chargement;
	
	public Avion(String modele, int masseVide, Chargeable[] chargement) {
		this.modele = modele;
		this.masseVide = masseVide;
		this.chargement = chargement;
	}

	public String getModele() {
		return modele;
	}

	public void setModele(String modele) {
		this.modele = modele;
	}

	public int getMasseVide() {
		return masseVide;
	}

	public void setMasseVide(int masseVide) {
		this.masseVide = masseVide;
	}
	
	public Chargeable[] getChargement() {
		return chargement;
	}

	public void setChargement(Chargeable[] chargement) {
		this.chargement = chargement;
	}

	public int getMasse() {
		int masse = this.getMasseVide();
		
		for (int i = 0; i < this.chargement.length; i++) {
		    masse = masse + this.chargement[i].getMasse();
		}
		
		return masse;
	}
}
