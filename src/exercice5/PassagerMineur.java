package exercice5;

public class PassagerMineur extends PassagerHumain {

	private int numeroAutorisation;
	
	public static final int MASSE_CLASSE_3 = 60;
	public static final int MASSE_CLASSE_2 = 50;
	public static final int MASSE_CLASSE_1 = 35;

	public PassagerMineur(String nom, String prenom, int sexe, int age, boolean fidele, int numeroAutorisation) {
		super(nom, prenom, sexe, age, fidele);
		this.numeroAutorisation = numeroAutorisation;
	}

	public int getNumeroAutorisation() {
		return numeroAutorisation;
	}

	public void setNumeroAutorisation(int numeroAutorisation) {
		this.numeroAutorisation = numeroAutorisation;
	}

	public int getMasse() {
		if(this.getAge() >= 15)
			return MASSE_CLASSE_3;
		else if(this.getAge() <= 10)
			return MASSE_CLASSE_1;
		else
			return MASSE_CLASSE_2;
	}
}
