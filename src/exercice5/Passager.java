package exercice5;

public abstract class Passager implements Chargeable {
	private String nom;
		
	public Passager(String nom) {
		this.nom = nom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
}
