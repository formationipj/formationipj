package exercice5;

public class PaletteFret implements Chargeable {
	
	private String type;
	private int masse;
	
	public PaletteFret(String type, int masse) {
		this.type = type;
		this.masse = masse;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getMasse() {
		return masse;
	}

	public void setMasse(int masse) {
		this.masse = masse;
	}
	
	
}
