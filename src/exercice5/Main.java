package exercice5;

public class Main {

	public static void main(String[] args) {

		// Cr�ation de passagers
		PassagerHumain passager1 = new PassagerHumain("Smith", "Jack", PassagerHumain.SEXE_HOMME, 45, false);
		PassagerHumain passager2 = new PassagerHumain("Smith", "Rachel", PassagerHumain.SEXE_FEMME, 40, false);
		PassagerHumain passager3 = new PassagerHumain("Dubois", "Claude", PassagerHumain.SEXE_AUTRE, 25, true);
		PassagerHumain passager4 = new PassagerHumain("Krop", "Jay", PassagerHumain.SEXE_HOMME, 28, true);
		
		PassagerMineur passagerMineur1 = new PassagerMineur("Smith", "Jonathan", PassagerHumain.SEXE_HOMME, 16, false, 56985);
		PassagerMineur passagerMineur2 = new PassagerMineur("Smith", "Claudia", PassagerHumain.SEXE_FEMME, 8, false, 96523);
		PassagerMineur passagerMineur3 = new PassagerMineur("Noloin", "M�lissa", PassagerHumain.SEXE_FEMME, 13, true, 11238);

		Animal animal1 = new Animal("M�dor", "Chien");
		Animal animal2 = new Animal("Nala", "Chat");
		
		// Cr�ation de fret
		PaletteFret palette1 = new PaletteFret("Pomme", 2478);
		PaletteFret palette2 = new PaletteFret("Pi�ces automobiles", 3479);
		
		// Cr�ation de valises
		Valise valise1 = new Valise(passager1, 18);
		Valise valise2 = new Valise(passager4, 20);
		Valise valise3 = new Valise(passagerMineur2, 12);

		
		// Cr�ation du tableau
		Chargeable[] chargement = { passager1, passager2, passager3, passager4, 
				passagerMineur1, passagerMineur2, passagerMineur3, 
				animal1, animal2,
				palette1, palette2,
				valise1, valise2, valise3 };
		
		// Cr�ation de l'avion
		Avion avion = new Avion("Airbus A220-300", 55000, chargement);
		
		System.out.println("Masse du passager1 (homme): " + passager1.getMasse());
		System.out.println("Masse du passager2 (femme): " + passager2.getMasse());
		System.out.println("Masse du passager3 (autre): " + passager3.getMasse());
		
		System.out.println("Masse du passager mineur 1 (16 ans): " + passagerMineur1.getMasse());
		System.out.println("Masse du passager mineur 2 (8 ans): " + passagerMineur2.getMasse());
		System.out.println("Masse du passager mineur 3 (13 ans): " + passagerMineur3.getMasse());

		System.out.println("Masse d'un animal : " + animal1.getMasse());
		
		System.out.println("Masse de l'appareil : " + avion.getMasse());

	}

}
