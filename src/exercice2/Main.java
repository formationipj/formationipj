package exercice2;

public class Main {

	public static void main(String[] args) {

		// Cr�ation de passagers
		Passager passager1 = new Passager("Smith", "Jack", Passager.SEXE_HOMME, 45, false);
		Passager passager2 = new Passager("Smith", "Rachel", Passager.SEXE_FEMME, 40, false);
		Passager passager3 = new Passager("Dubois", "Claude", Passager.SEXE_AUTRE, 25, true);
		Passager passager4 = new Passager("Krop", "Jay", Passager.SEXE_HOMME, 28, true);

		// Cr�ation du tableau
		Passager[] passagers = { passager1, passager2, passager3, passager4 };
		
		// Cr�ation de l'avion
		Avion avion = new Avion("Airbus A220-300", 55000, passagers);
		
		System.out.println("Masse du passager1 (homme): " + passager1.getMasse());
		System.out.println("Masse du passager2 (femme): " + passager2.getMasse());
		System.out.println("Masse du passager3 (autre): " + passager3.getMasse());
		
		System.out.println("Masse de l'appareil : " + avion.getMasse());

	}

}
