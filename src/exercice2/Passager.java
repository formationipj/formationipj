package exercice2;

public class Passager {
	private String nom;
	private String prenom;
	private int sexe;
	private int age;
	private boolean fidele;
	
	public static final int SEXE_HOMME = 0;
	public static final int SEXE_FEMME = 1;
	public static final int SEXE_AUTRE = 2;
	
	public static final int MASSE_HOMME = 80;
	public static final int MASSE_FEMME = 70;
	public static final int MASSE_AUTRE = 75;
	
	
	public Passager(String nom, String prenom, int sexe, int age, boolean fidele) {
		this.nom = nom;
		this.prenom = prenom;
		this.sexe = sexe;
		this.age = age;
		this.fidele = fidele;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getSexe() {
		return sexe;
	}

	public void setSexe(int sexe) {
		this.sexe = sexe;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public boolean isFidele() {
		return fidele;
	}

	public void setFidele(boolean fidele) {
		this.fidele = fidele;
	}
	
	public int getMasse() {
		if(this.sexe == SEXE_HOMME)
			return MASSE_HOMME;
		else if(this.sexe == SEXE_FEMME)
			return MASSE_FEMME;
		else
			return MASSE_AUTRE;
	}
	
	
}
