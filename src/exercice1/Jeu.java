package exercice1;

import java.util.Random;
import java.util.Scanner;

public class Jeu {

	public static void main(String[] args) {
		
		// G�n�ration du nombre al�atoire
		Random random = new Random();
		int nombre = random.nextInt(100) + 1;
		
		// Cr�ation du Scanner pour la saisie utilisateur
		Scanner scanner = new Scanner(System.in);
		
		// Initialisation du nombre de tentatives � 0
		int tentatives = 0;
		
		// Pour stocker la saisie utilisateur
		int saisie;
		
		do {
			// R�cup�ration de la saisie utilisateur
			System.out.println("Votre proposition : ");
			saisie = scanner.nextInt();
			
			// On incr�mente le nombre de tentatives
			tentatives = tentatives + 1; // ou tentatives += 1 ou tentatives++
			
			if(saisie < nombre) {	// Saisie trop petite
				System.out.println("Trop petit");
			}
			else if(saisie > nombre) { // Saisie trop grande
				System.out.println("Trop grand");
			}
		} while(saisie != nombre);	// On boucle tant que l'utilisateur n'a pas saisi le bon nombre
		
		// Arriv� ici, l'utilisateur a trouv� le bon nombre, on affiche le message de f�licitations
		System.out.println("Bravo ! Trouv� en " + tentatives + " tentatives.");
		
		// Fermeture du scanner
		scanner.close();
	}

}
